const http = require("http");
const fs = require("fs");
const server = http.createServer((req, res) => {
  fs.readFile("./views/index.html", (err, data) => {
    if (err) {
      res.end();
    } else {
      res.end(data);
    }
  });
});
server.listen(4000, "localhost", () => {
  console.log("listening for requests on port 4000");
});
